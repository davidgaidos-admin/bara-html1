
var win = Ti.UI.currentWindow;



Ti.include('app_functions.js');

Ti.include('global_ui.js');

var menuBtn = Ti.UI.createButton({
	title: L('X'),
	color:'black',
	backgroundImage: 'rundKnapp.png',
	height: 60,
	width: 60,
	top: '1%',
	left: '1%',
	font: {fontSize: 22}
});
win.add(menuBtn);



menuBtn.addEventListener('click', function(e){
	//!!TODO, här ska vi visa en meny,
	// XXX i nuläget använder vi denna knapp för att stänga fönstret
	// detta är bara tillfälligt för att vi ska kunna testa layouten (så den är korrekt)
	win.close();
});

//blue
var anim = Ti.UI.createAnimation({
	top:175,
	left:'0dp',
	opacity:0.3,
	duration:1500,    
});

var view = Ti.UI.createView({
	height:'5dp',
	width:'50dp',
	backgroundColor:'#024471',
	borderRadius: 10,
	top:175,
	left:-50,
	opacity:0.1,
//	duration:30,
	zIndex:3
});
win.add(view);


// orange
var anim4 = Ti.UI.createAnimation({
	top:'80dp',
	right:'0dp',
	opacity:0.3,
	duration:1500
});
var view4 = Ti.UI.createView({
	height:'5dp',
	width:'60dp',
	opacity:0.1,
//	duration:30,
	backgroundColor:'#FA980F',
	borderRadius: 10,
	top:'80dp',
	right:'-60dp',
	zIndex:4
	
});
win.add(view4);
//red
var anim2 = Ti.UI.createAnimation({
	bottom:'125dp',
	right:'0dp',
	opacity:0.1,
	duration:1500
});

var view2 = Ti.UI.createView({
	height:'5dp',
	width:'50dp',
	opacity:0.3,
//	duration:30,
	backgroundColor:'#FF1212',
	borderRadius: 10,
	bottom:'125dp',
	right:'-50dp',
	zIndex:1
});
win.add(view2);

//yellow
var anim3 = Ti.UI.createAnimation({
	bottom:'70dp',
	left:'85dp',
	opacity:0.3,
	duration:1500
});

var view3 = Ti.UI.createView({
	height:'5dp',
	width:'50dp',
	backgroundColor:'#95C120',
	opacity:0.1,
//	duration:30,
	borderRadius: 10,
	bottom:'70dp',
	left:'-50dp',
	zIndex:2
	
});
win.add(view3);

/*
var image = Ti.UI.createImageView({
	image:'/images/RhymeScript5rsx.png',
	width:'100dp',
	height:'100dp',
	top:'140dp',
	left:'120dp',
//	duration:30,
	zIndex:5
});
win.add(image);
*/
var image = Ti.UI.createImageView({
	image:'/images/NightNavNeoSign.png',
	width:'100%',
	height:'100%',
	top:'60dp',
	//left:'17dp',
//	duration:30,
});
win.add(image);
//var btn = Ti.UI.createButton({
//	title:'Klicka',
//	height:'50dp',
//	width:'100dp',
//	bottom:'10dp'
//});
//win.add(btn);


//btn.addEventListener('click',function(){
	view.animate(anim);
	view2.animate(anim2);
	view3.animate(anim3);
	view4.animate(anim4);
//	image.animate(anim2);
	
//});

//ICONS SOCIAL MEDIA//////////
var image = Ti.UI.createImageView({
	image:'/images/facebook_icon.png',
 	url:'http://www.google.com',
	width:'30dp',
	height:'30dp',
	bottom:'50dp',
	left:'75dp',
	opacity: 0.8,
});
win.add(image);

var image = Ti.UI.createImageView({
	image:'/images/twitter_icon.png',
	url:'http://www.google.com',
	width:'30dp',
	height:'30dp',
	bottom:'50dp',
	left:'200dp',
	opacity: 0.8,
});
win.add(image);
