/**
  **
   ** innehåller fördefinierade fonter och färger
   **
   **  >> finns i alla filer även app.js
  **
**/

//
// ** colors **
//
var blackFontColor = '#000',
	grayFontColor = '#7F7F7F',
	greenFontColor = '#528a27',
	yellowFontColor = '#f8b926',
	blueFontColor = '#46a8fd',
	brownFontColor = '#966805',
	whiteFontColor = '#fff',
	winBgColor = '#f6f6f6';

//
// ** fonts **
//
if (OS_is_Android) {
	// on Android, use the "base name" of the file (name without extension)
	var defFontFamily = 'sans-serif';

} else {
	// use the friendly-name on iOS
	var defFontFamily = 'Helvetica Neue';

};

var fonts = [];
for (var i = 10; i < 48; i++) {
	fonts[i] = [];
	fonts[i]['normal'] = 	{	fontFamily: 'Helvetica Neue',			fontSize: i+'sp',	fontWeight: 'normal',	fontStyle: 'normal' };
	fonts[i]['bold'] =	 	{	fontFamily: 'Helvetica Neue',		fontSize: i+'sp',	fontWeight: 'bold',		fontStyle: 'normal' };
}

font9 	= {	fontFamily:defFontFamily,	fontSize: '9sp',		fontWeight: 'normal',	fontStyle: 'normal' };

font11	= {	fontFamily:defFontFamily,	fontSize: '11sp',	fontWeight: 'normal',	fontStyle: 'normal' };

font12	= {	fontFamily:defFontFamily,	fontSize: '12sp',	fontWeight: 'normal',	fontStyle: 'normal' };

font14	= {	fontFamily:defFontFamily,	fontSize: '14sp',	fontWeight: 'normal',	fontStyle: 'normal' };

font24	= {	fontFamily:defFontFamily,	fontSize: '24sp',	fontWeight: 'normal',	fontStyle: 'normal' };

