var win = Ti.UI.currentWindow;

var OS_is_Android = Ti.Platform.osname == "android";

win.backgroundColor = '#fff';

Ti.include('fonts_and_colors.js');

var menuBtn = Ti.UI.createButton({
	title: L('X'),
	color:'black',
//	backgroundImage: 'rundKnapp.png',
	height: 60,
	width: 60,
	top: '1%',
	left: '1%',
	font: {fontSize: 22}
});
win.add(menuBtn);



menuBtn.addEventListener('click', function(e){
	//!!TODO, här ska vi visa en meny,
	// XXX i nuläget använder vi denna knapp för att stänga fönstret
	// detta är bara tillfälligt för att vi ska kunna testa layouten (så den är korrekt)
	win.close();
});

/*
function showMyNumber(nummerIn,textIn){
	
	alert(nummerIn + textIn);
}

showMyNumber(0,'Hej');
*/
//Bild
//Titel
//Beskrivande text
//Has child
//html länk


var myData = [];
var curr = 0;

function addToMyData(myImage, myTitle, myDesc, myHtml, myColor){
	
	var row = Ti.UI.createTableViewRow({
//		top:'60dp',
		height:'55dp',
//		backgroundColor:'#000',
		//hasChild:true,
		receptURL:myHtml,
		receptTitle:myTitle
	});
	
	if(myColor == 0){
		row.backgroundColor = '#000';	// bakgrund som e grå inte grönn
	}else if(myColor == 1){
		row.backgroundColor = '#89df99';
		row.height = '100dp';
	}else if(myColor == 2){
		row.backgroundColor = '#c4eecc';
	}
	
	var image = Ti.UI.createImageView({
		image:myImage,
		width:'46dp',
		height:'46dp',
		left:10
	});
	row.add(image);
	
	var title = Ti.UI.createLabel({
		text:myTitle,
		height:'auto',
		width:'auto',
		left:'70dp',
		top:'10dp',
		color:'#ffffff',
		font:{
			fontFamily:'Capture it',
		//	fontWeight:'bold',
			fontSize:'15sp'
		}
	});
	row.add(title);
	
	var desc = Ti.UI.createLabel({
		text:myDesc,
		height:'auto',
		width:'auto',
		left:'70dp',
		right:'35dp',
		textAlign:'left',
		top:'30dp',
		color:'#CC6633',	// FA9507	FCB021
		font:{
			fontFamily:'Helvetica Neue',
			fontSize:'10sp'
		}
	});
	row.add(desc);
/*	
	var line = Ti.UI.createView({
		height:'1dp',
		width:'100%',
		backgroundColor:'#1C8CFC',
		bottom:'1dp'
	});
	row.add(line);
*/	
	var line3 = Ti.UI.createView({
		left: '10dp',
		right: '10dp',
		height:'0.5dp',
	//	width:'100%',
		backgroundColor:'#FFFFFF',
		bottom:'-0.5dp'
	});
	row.add(line3);
	
	var line2 = Ti.UI.createView({
		left: '10dp',
		right: '10dp',
		height:'0.5dp',
	//	width:'100%',
		backgroundColor:'#FFFFFF', ///LINJE två samma skit som linje 1
		bottom:'0dp'
	});
	row.add(line2);
	
	myData[curr] = row;
	curr++;
	
}//Slut på funktionen addToData

var tableView = Ti.UI.createTableView({
	top:'60dp',
	separatorStyle:Titanium.UI.iPhone.TableViewSeparatorStyle.NONE
});
win.add(tableView);

tableView.addEventListener('click',function(e){
	
	var win1 = Ti.UI.createWindow({
		url:'subWin.js',
//		backgroundColor:greenFontColor,
//		color:greenFontColor,
//		backgroundImage:'/images/black-transparent-background.png',
//		backgroundImage:'/images/black0_7transparent.png',
		title:e.rowData.receptTitle,
		chapters:e.rowData.receptURL
		
});



if (OS_is_Android){
	win1.open();
} else{
	
	win1.open();
	/*
	var navWin = Ti.UI.iOS.createNavigationWindow({
		
		window: win1
	});

	
	win1.navWin = navWin;

	
	navWin.open({modal:true});
	*/
}
});

addToMyData('/images/etage1.jpg','Etage Stortorget (Nattklubb)','House Schlager Svenska hits & Klassiker','Views/ch01.html',0);
addToMyData('/images/hipp1.jpg','Ribban','Schlager,hip hop,RnB,House','Views/ch02.html',0);
addToMyData('/images/breeze1.png','Breeze','Rnb, hip hop, Svenska hits & Klassiker ','Views/ch03.html',0);
addToMyData('/images/klubbf.png','Klubb F','House','Views/ch04.html',0);
addToMyData('/images/mattsons.png','Mattsons ','htp://en.wikipedia.org/wiki/Wonderwall_(song)','Views/ch05.html',0);
addToMyData('/images/Chords_The_dude_RGB-568x568.jpg','Chords The Dude','Wikipedia, the free encyclopedia','Views/ch06.html',0);
addToMyData('/images/chris rene.jpg','Chris Rene - Young Homie','Wikipedia, the free encyclopedia, debut EP IM RIGHT HERE','Views/ch07.html',0);
addToMyData('/images/eminem-and-royce-da-59-fast-lane.jpg','Eminem, Royce Da 59 - Fast','Wikipedia, the free encyclopedia, http://www.youtube.com/watch?v=rJOsjP33nF4','Views/ch08.html',0);
addToMyData('/images/Eminem_Royce_Da_5_9_Ft_Bruno_Mars_Lighters.jpg','Eminem - Lighters ft. Bruno','Wikipedia, the free encyclopedia, http://www.youtube.com/watch?v=YWt4wmZ_EMI','Views/ch09.html',0);
addToMyData('/images/bluegrass.jpg','Linus Svenning - Impossible','(James Arthur Acoustic Live Cover) http://www.youtube.com/watch?v=sumtKqdScCA&list=PLDVXElTcqpv4b9pdBzihKTodhQrzIuBXr','Views/ch10.html',0);
addToMyData('/images/bluegrass.jpg','Linus Svenning - Impossible','(James Arthur Acoustic Live Cover) http://www.youtube.com/watch?v=sumtKqdScCA&list=PLDVXElTcqpv4b9pdBzihKTodhQrzIuBXr','Views/ch10.html',0);


tableView.data = myData;
