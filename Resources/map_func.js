Ti.include("settings.js");

function getMapDataFromLocalFile(){
	var fileName = 'map.json';
	//vi anger en sökväg och ett namn till vår json-fil
	var file = Titanium.Filesystem.getFile(Titanium.Filesystem.resourcesDirectory + fileName);

	//vi läser in jsondata från den lokala filen
	var dataFromFile = file.read().text;

	try {
		//vi gör om json-datan till en array (med parse-kommandot)
		var jsonData = JSON.parse(dataFromFile);

		var pinArray = [];

		// alert(jsonData[0].adress);
		for (var i = 0; i < jsonData.length; i++) {
			Ti.API.info(jsonData[i].latitude + '  ' + jsonData[i].longitude);

			pinArray[i] = Map.createAnnotation({
				latitude: jsonData[i].latitude,
				longitude: jsonData[i].longitude,
				title: jsonData[i].adress,
				subtitle: jsonData[i].phone,
				pincolor: Map.ANNOTATION_GREEN,
				// leftButton: Titanium.UI.iPhone.SystemButton.DISCLOSURE,
				// rightButton: Titanium.UI.iPhone.SystemButton.DISCLOSURE,
				myid: i // Custom property to uniquely identify this annotation.
			});
		}

		mapview.setAnnotations(pinArray);
	} catch (err){
		//det gick inte att "parsa" json-filen
		alert('JSON-parse error!\n' + err.message);
	}
}


function getJsonFromHttp(){
			//	http://www.woodesoft.com/utbildning/map.json
	var url = "http://www.woodesoft.com/utbildning/map.json";		
	var client = Ti.Network.createHTTPClient({
		// function called when the response data is available
		onload: function(e) {
			Ti.API.info("Received text: " + this.responseText);
			alert('JSON-success');

			var jsonData = JSON.parse(this.responseText);
			

			var pinArray = [];

			// alert(jsonData[0].adress);
			for (var i = 0; i < jsonData.length; i++) {
				Ti.API.info(jsonData[i].latitude + '  ' + jsonData[i].longitude);

				pinArray[i] = Map.createAnnotation({
		//			Geop: jsonData[i].Geop,
					latitude: jsonData[i].latitude,
					longitude: jsonData[i].longitude,
					title: jsonData[i].klubb,
					subtitle: jsonData[i].klubb,
		//			subtitle: jsonData[i].phone,
					pincolor: Map.ANNOTATION_GREEN,
					// leftButton: Titanium.UI.iPhone.SystemButton.DISCLOSURE,
					// rightButton: Titanium.UI.iPhone.SystemButton.DISCLOSURE,
					myid: i // Custom property to uniquely identify this annotation.
				});
			}

			mapview.setAnnotations(pinArray);
		},
		// function called when an error occurs, including a timeout
		onerror: function(e) {
			Ti.API.debug(e.error);
			alert('error');
		},
		timeout: 35000 // in milliseconds
	});

	// Prepare the connection.
	client.open("GET", 'https://api.parse.com/1/classes/Map/');
	
	client.setRequestHeader("X-Parse-Application-Id", "4h2n9RVvXBVmrFDdSRRg9ignrT7435y6cRFNMTQZ");
	client.setRequestHeader("X-Parse-REST-API-Key", "a1bC8mfwSPfkeFB9IhNwCm6uOgO05M9XiYRN5md3");

	// Send the request.
	client.send();
}
