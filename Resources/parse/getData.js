Ti.include("settings.js");

var win = Ti.UI.currentWindow;

var jsonArray = [];

var BackImage = Ti.UI.createImageView({
		height:'568dp',	
		width:'320dp',		
		hasChild:true,
	});	
	win.add(BackImage);


var personerTableView = Ti.UI.createTableView({
	top: "43dp",
	bottom: "0dp",
	backgroundColor:'transparent',
	editable: true,
//	search: search,
	filterAttribute: 'searchVal'
}); 

// Radera event på tableview
personerTableView.addEventListener("delete", function(e) {
	// Gör delete mot ditt object i din class
	
	c = Titanium.Network.createHTTPClient();
	c.setTimeout(25000);
	c.onload = function() {
		alert("Raderad");
	};
	c.onerror = function(e) {
	//	alert(e);
	};
	
	c.open('DELETE', "https://api.parse.com/1/classes/list/"+e.rowData.objectId);
	
	c.setRequestHeader("X-Parse-Application-Id", "4h2n9RVvXBVmrFDdSRRg9ignrT7435y6cRFNMTQZ");
	c.setRequestHeader("X-Parse-REST-API-Key", "a1bC8mfwSPfkeFB9IhNwCm6uOgO05M9XiYRN5md3");
	
	c.send();
});

personerTableView.addEventListener("click", function(e) {
	// Öppna nytt fönster och skicka med objectId från raden
	moreInfoWin = Ti.UI.createWindow({
		url: "moreInfo.js",
		name: e.rowData.name,
		objectId: e.rowData.objectId,
		backgroundColor: "#ffffff",
	});
	Ti.UI.currentTab.open(moreInfoWin);
	

});


win.add(personerTableView);


var reloadButton = Ti.UI.createButton({
	title: "Reload",
//	backgroundColor: 'black',
	top: "5dp",
	right: "1dp",
	borderRadius: 5,	
	font:{fontFamily:'Capture it', fontSize:'20sp'}	

});
reloadButton.addEventListener("click", function(e) {
	getData();
});
win.add(reloadButton);


var personerTableData = [];
function makeRow(arrayID, objectId, update, name, tidning, Adress, Kundnr, bgColor, date, GeoP)
{
	row = Ti.UI.createTableViewRow({
		height: "60dp",
		name: name,
		objectId: objectId,
		backgroundColor: bgColor,
		tidning: tidning,
		updatelabel: date,
		searchVal: Adress
	});
	
	updatelabel = Ti.UI.createLabel({
		text: date,
		left: "3dp",
		top: "5dp",
//		width: "60dp",
		font:{fontSize: '12sp'}
//		font:{fontSize:'10sp', fontWeight:'bold'},
	});	
	row.add(updatelabel);
	
	tidninglabel = Ti.UI.createLabel({
		text: tidning,		
		left: "60dp",
		bottom: "5dp",
		color:'#75B0EF',
		font:{fontFamily:'Capture it', fontSize:'20sp', fontWeight:'bold'}
	});	
	row.add(tidninglabel);
	
	Adresslabel = Ti.UI.createLabel({
		text: Adress,
		left: "130dp",
		font:{fontFamily:'Capture it', fontSize:'15sp'}
	});	
	row.add(Adresslabel);
	
	Kundnrlabel = Ti.UI.createLabel({
		text: Kundnr,
		right: "5dp",
		top: "5dp",
		font:{fontSize:'13sp', fontWeight:'bold'}, //fontFamily:'Capture it', 
		bubbleParent: false,
		arrayID: arrayID
	});	
	row.add(Kundnrlabel);
	
	Kundnrlabel.addEventListener("click", function(e) {
		// Öppna nytt fönster och skicka med objectId från raden
//		alert(jsonArray[this.arrayID].Kundnr);
		moreInfoWin = Ti.UI.createWindow({
			url: "saknadTidning.js",
			Kundnr: jsonArray[this.arrayID].Kundnr,
	//		name: e.rowData.name,
	//		objectId: e.rowData.objectId,
			
			
			backgroundColor: "#ffffff"
		});
		Ti.UI.currentTab.open(moreInfoWin);
	
	});
	
	personerTableData.push(row);
	
}


// Funktion för att hämta data
function getData()
{
	personerTableData = [];
	
	c = Titanium.Network.createHTTPClient();
	c.setTimeout(25000);
	c.onload = function() {
		json = JSON.parse(this.responseText);
		jsonArray = json.results;
		// Alla hämtade rader finns i json.results
		for(i = 0;i < json.results.length;i++)
		{
			makeRow(i, json.results[i].objectId, json.results[i].update, json.results[i].name, json.results[i].tidning, json.results[i].Adress,
					   json.results[i].Kundnr, json.results[i].bgColor, json.results[i].date, 
					   json.results[i].GeoP);			
		}
		
		personerTableView.setData(personerTableData);
	};
	c.onerror = function(e) {
		alert(e);
	};	
	
	NrID = "9287";
	
	c.open('GET', 'https://api.parse.com/1/classes/list/?where={"DistriktNr": "'+NrID+'"}');
//	c.open('GET', 'https://api.parse.com/1/classes/list/?order=date');
	
	c.setRequestHeader("X-Parse-Application-Id", "4h2n9RVvXBVmrFDdSRRg9ignrT7435y6cRFNMTQZ");
	c.setRequestHeader("X-Parse-REST-API-Key", "a1bC8mfwSPfkeFB9IhNwCm6uOgO05M9XiYRN5md3");
	
	c.send();
}
getData();