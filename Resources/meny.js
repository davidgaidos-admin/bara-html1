var win = Ti.UI.currentWindow;


var OS_is_Android = Ti.Platform.osname == "android";

Ti.include('app_functions.js');
Ti.include('fonts_and_colors.js');

var nameview = Titanium.UI.createView({
	height: 80,
	top: 0,
	//backgroundColor: '#fff'
});
win.add(nameview);

var gamename = Titanium.UI.createLabel({
	text: L ('Malmö City Longboard'), 
	font:{fontSize:20, fontWeight:'bold'},
	bottom: '10 %'
});

nameview.add(gamename);

//vi skapar en "hållare" för de fyra menyknapparna, för att vi enklare ska kunna flytta de
// vi gör även layout:en titll vertical, så palceras knapparna med jämnt och fint avstånd till varandra
var buttonHolder = Ti.UI.createView({
	top: '15%',
	height: Ti.UI.SIZE,
	width: '100%',
	layout: 'vertical',
});
win.add(buttonHolder);

/*

var btn1 = Ti.UI.createButton({
	title: L ('Cruisin'), //'Game 1',
	top: '20 %',
	//bottom: '67 %',
//	backgroundColor: '#fff',
	backgroundImage: '/images/buttonStyle.png',
	height: 100,
	width: 100,
	left: '15 %',
	color:'white',
	font: {
		fontFamily: 'Trebuchet MS',
		fontSize: 12
		}	
	
});

win.add(btn1);



btn1.addEventListener('click', function(e){
	var win1 = Titanium.UI.createWindow({
		url: 'cruising.js',
		//backgroundImage: 'game1.png',
		//barColor: '#BD85E7',
		navBarHidden: true,
		backgroundColor: '#fff'
	});
	
	if (OS_is_Android){
    		
    		//win2.title = '';
    		
    		win1.open();
    		
    	} else {
    		
    	win.navWin.openWindow(win1);
    	
    	}

});

*/
var btn2 = Ti.UI.createButton({
	title: L ('Downhill'), //'Game 2',
	top: '20 %',
	//bottom: '67 %',
	//backgroundImage: 'rundKnapp.png',
	backgroundImage: 'images/IkonLongbord.png',
	height: 100,
	width: 100,
//	right: '15 %',
	//left: '45%',
	font: {
		fontFamily: 'Trebuchet MS',
		fontSize: 12
		}	
	
});

buttonHolder.add(btn2);


btn2.addEventListener('click', function(e){
	var win2 = Titanium.UI.createWindow({
		url: 'chapters.js',
		title:'Downhill',
//		backgroundImage:'/images/black0_7transparent.png',
		//barColor: '#BD85E7',
		navBarHidden: true,
	//	backgroundColor: '#FFF'

	});
	
	if (OS_is_Android){
    		
    		win2.title = 'Downhill';
    		
    		win2.open();
    		
    	} else {
    		
    	win.navWin.openWindow(win2);
    	
    	}

});
/*
var btn3 = Ti.UI.createButton({
	title: L ('Garage'), // 'Game 3',
	top: '40 %',
	//bottom:' 47 %',
	//backgroundImage: 'rundKnapp.png',
	backgroundImage: '/images/buttonStyle.png',
//	backgroundColor: 'green',
	height: 100,
	width: 100,
	left: '15 %',
	font: {
		fontFamily: 'Trebuchet MS',
		fontSize: 12,
		color: 'black'
		}	
	
});

win.add(btn3);


btn3.addEventListener('click', function(e){
	var win3 = Titanium.UI.createWindow({
	//	url: 'game3.js',
		url: 'garage.js',
		//backgroundImage: 'game1.png',
		//barColor: '#BD85E7',
		navBarHidden: true,
		backgroundColor: '#FFFD6C'
	});
	
	if (OS_is_Android){
    		
    		//win2.title = '';
    		
    		win3.open();
    		
    	} else {
    		
    	win.navWin.openWindow(win3);
    	
    	}

});
*/
var btn4 = Ti.UI.createButton({
	title: L ('Filmer/inlägg'), //'Game 4',
	top: 15,
	//bottom: '47 %',
	//backgroundImage: 'rundKnapp.png',
	backgroundImage: '/images/buttonStyle.png',
	height: 100,
	width: 100,
	font:{fontSize:12, fontWeight:'bold'},
	
});

buttonHolder.add(btn4);


btn4.addEventListener('click', function(e){
	var win4 = Titanium.UI.createWindow({
		url: 'contact.js',
		//backgroundImage: 'game1.png',
		//barColor: '#BD85E7',
		navBarHidden: true,
		backgroundColor: '#fff'
	});
	
	if (OS_is_Android){
    		
    		win4.open();
    		
    	} else {
    		
    	win.navWin.openWindow(win4);
    	
    	}

});


var btn5 = Ti.UI.createButton({
//	title: L ('Actionbolaget'), //'Game 4',
	top: 15,
	//bottom: '47 %',
	//backgroundImage: 'rundKnapp.png',
	backgroundImage: '/images/Ac.png',
	height: 100,
	width: 100,
//	right: '35%',
	font:{fontSize:12, fontWeight:'bold'},
	
});

buttonHolder.add(btn5);


btn5.addEventListener('click', function(e){
	var win5 = Titanium.UI.createWindow({
		url: 'actionbolaget.js',
		//backgroundImage: 'game1.png',
		//barColor: '#BD85E7',
		navBarHidden: true,
		backgroundColor: '#fff'
	});
	
	if (OS_is_Android){
    		
    		win5.open();
    		
    	} else {
    		
    	win.navWin.openWindow(win5);
    	
    	}

});