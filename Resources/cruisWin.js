var win = Ti.UI.currentWindow;

var menuBtn = Ti.UI.createButton({
	title: L('X'),
	color:'black',
	backgroundImage: 'rundKnapp.png',
	height: 60,
	width: 60,
	top: '1%',
	left: '1%',
	font: {fontSize: 22}
});
win.add(menuBtn);

win.backButtonTitle = 'NN';

//var receptLinkSomKomFranForraFonstret = win.recept;

var webView = Ti.UI.createWebView({
	top:'60dp',
	url:win.cruising
});


menuBtn.addEventListener('click', function(e){
	//!!TODO, här ska vi visa en meny,
	// XXX i nuläget använder vi denna knapp för att stänga fönstret
	// detta är bara tillfälligt för att vi ska kunna testa layouten (så den är korrekt)
	win.close();
});
win.add(webView, menuBtn);
