
/**
 *  	Denna fil kan kopieras till alla era projekt ni vill ha en highscore lista i
 * 	den tar hand om spara, läsa och radera från en highscore tabell i en dtatabas lokalt i appen
 */

//en array vi ska spara alla rader från databasen
var allHighscores = [];

//här börjar vi med att skapa själva databasen (om det behövs)
var db = Ti.Database.open('gamedata');

if (OS_is_iOS){
	//ingen iCloud backup
	db.file.remoteBackup = false;
}

//denna kod är bra at ha under testning, ifall man behöver göra om strukturen i databasen
var delete_database = false;//!!TODO, ska vara false i skarp version
if (delete_database){
	//execute() metoden används för att "skicka" databasfrågor till databasen
	// här säger vi till databasen att den ska ta bort tabellen med namnet highscores
	db.execute('DROP TABLE IF EXISTS highscores');
}

//här skapar vi en tabell med namnet highscores och med kolumnerna name, score, diff, time och vi sätter
// samtidigt vilken typ av data det är TEXT eller INTEGER
db.execute('CREATE TABLE IF NOT EXISTS highscores (name TEXT, score INTEGER, diff INTEGER, level TEXT, time INTEGER)');

//vi måste alltid stänga databasen
// är den inte stängd, går den inte att öpnna igen, då den är låst
// för ändring, så glöm aldrig denna rad
db.close();


// function för att spara highscore
function saveHighscore(highscoreData) {

	//till funktonen skickar vi med data som vi kan läsa av från variablen: highscoreData

	// det är en dictionary, som ser ut så här:
	/*
	  highscoreData = {
	  	name: 'Micke',
	  	score: 0,
	  	diff: 1,
	  	level: '2x2',
	  	time: 56000
	  }
	 */
	var db = Ti.Database.open('gamedata');

	if (OS_is_iOS){
		//ingen iCloud backup
		db.file.remoteBackup = false;
	}

	//vi skriver ut i loggen, hur "frågan" till databasen ser ut
	// lbir lättare och se ifall vi missat nån fnutt ' eller "
	Ti.API.info('INSERT INTO highscores VALUES("'+highscoreData.name+'", '+ highscoreData.score+', '+highscoreData.diff+', "'+highscoreData.level+ '", '+highscoreData.time+')');

	//denna rad säger åt databasen att lägga till en rad i higscores tabellen
	// och vi anger värden för varje kolumn
	// notera att vi mpåste lista värden i rätt ordning
	// i denna "fråga"
	db.execute('INSERT INTO highscores VALUES("'+highscoreData.name+'", '+ highscoreData.score+', '+highscoreData.diff+', "'+highscoreData.level+ '", '+highscoreData.time+')');

	//stäng db
	db.close();
}

//här läser vi in data från databasen
function loadHighscores(whichDiff, callback){

	//vi tömme rev tidigare värden vi läst in
	allHighscores = [];

	//öppna db
	var db = Ti.Database.open('gamedata');

	if (OS_is_iOS){
		//ingen iCloud backup
		db.file.remoteBackup = false;
	}
	//vi kollar att vi fått ätt värden in i funktionen (så inte appen kraschar ifall vi gör fel)
	if (whichDiff === undefined) {
		whichDiff = 0;
	}
	if (whichDiff === 'all') {
		whichDiff = 0;
	}
	// om whichDiff är noll, hämta alla highscores
	if (whichDiff == 0){
		var dbRows = db.execute('SELECT * FROM highscores ORDER BY score DESC');

	}else{
		// hämta bara för den svårighetsgraden vi valt

		//vi "väljer" alla data (*) från highscores tabellen där diff kolumnen är lika med whichDiff
		// och vi sorterar resultatet på poäng kolumnen i fallande ordning
		var dbRows = db.execute('SELECT * FROM highscores WHERE diff = '+whichDiff+' ORDER BY score DESC'); //TODO lägg till WHERE
	}

	//dbRows innehåller nu alla rader som matchde frågan till databasen
	// man kan fråga resultet med metoden isValidRow() om det finns en rad
	// och vi kommer köra while-loopen så långa det finns värden i dbRows
	while (dbRows.isValidRow()) {

		//vi skapar en tilfällig dictionary
		var highscoreData = {
			name: dbRows.fieldByName('name'),//'Micke',
			score: dbRows.fieldByName('score'),//0,
			diff: dbRows.fieldByName('diff'),//4,
			level: dbRows.fieldByName('level'),//'2x2',
			time: dbRows.fieldByName('time')
		};

		Ti.API.info(highscoreData);

		//som vi sen pushar in i array:en med alla highscores
		allHighscores.push(highscoreData);

		//glöm inte denna rad, annars får ni en evighetsloop
		// som gör att appen tillslut slutar fungera
		// TODO test gärna att kommentera bort raden under och gå in på highscore listan
		dbRows.next();
	}

	//när vi är färdiga med dbRows, stäng / resna bort minnet för alla raderna
	dbRows.close();

	//stäng sen db
	db.close();

	//om vi har skickat in en callback funktion till loadHighscores
	// så komemr vi nu köra den
	if (callback){
		callback(whichDiff);
	}
}

//här tar vi bort highscores antigen alla ('all') eller så anger man en siffra fr vilken
// svårighetsgrade som ska tas bort
function deleteHighscores(which) {
	var db = Ti.Database.open('gamedata');

	if (OS_is_iOS){
		//ingen iCloud backup
		db.file.remoteBackup = false;
	}

	if (which == 'all') {
		// vi kör kommandot DELETE på highscores tabellen
		// denna rad tömemr hela tebellen från dess data, strukturen är dock kvar
		db.execute('DELETE FROM highscores');

	} else {
		// denna rad tar bort alla rader där diff = which-värdet
		db.execute('DELETE FROM highscores WHERE diff = ' + which);
	}

	db.close();
}


/*
saveHighscore({
	name: 'Micke',
  	score: Math.round(Math.random()*8000),
  	diff: 4,
  	level: '2x2',
  	time: Math.round(Math.random()*120000)
});
*/

// deleteHighscores('all');

// loadHighscores();




