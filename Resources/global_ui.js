
//i denna fil lägger vi bara UI-objekt som ska finnas i alla fönster
// ...där denna fil inkluderas med Ti.include()

//vi gör en bakåt knapp nu när vi inte längre har en navBar
var backBtn = Ti.UI.createButton({
	title: 'X',
	top: dp(20),
	left: dp(20)
});
//win.add(backBtn);

backBtn.addEventListener('click', function(e){
	//när vi trycker på den så stängs det fönstret som läste in denna fil
	// det håller appen koll på och stänger därmed rätt fönster
	win.close();
});

//vi skapar eäven en "titel" till varje fönster, så användaren vet var den är
// text property:n till denna label, sätts i respektive fönster
// då vi i global_ui filen inte vet vad det ska stå
var winTitleLbl = Ti.UI.createLabel({
	text: '',
	top: dp(18),
	font: fonts[32]['normal'],
	color: blackFontColor

});
win.add(winTitleLbl);


